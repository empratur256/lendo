##lendo test
نیازمندی های این پروژه
```
docker compose
php7.3
mysql
composer
```
اگر با داکر پروژه را اجرا می کنید همان داکر کافیست
ابتدا با استفاده از دستور زیر در ترمینال پروژه را دریافت کنید

```
git clone https://gitlab.com/empratur256/liateam.git
```
درفایل
```
docker-compose.yml
```

در قسمت زیر اطلاعات دیتا بیس را به سلیقه خود تغییر دهید
```
environment:
      MYSQL_DATABASE: test
      MYSQL_USER: root
      MYSQL_PASSWORD: pass
      MYSQL_ROOT_PASSWORD: pass
```
حال اگر بر روی سیستم داکر نصب دارید دستور زیر را وارد کنید
```
docker-compose up -d
```

حال با زدن دستور زیر پکیج های آن را نصب کنید
```
composer install
```
برای داکر
```
docker-compose exec app composer install
```
حال اطلاعات دیتابیس خود را در .env وارد کنید
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
حال برای ساخت دیتابیس دستور زیر را وارد کنید
```
php arrisan migrate
```
برای داکر
```
docker-compose exec app php arrisan migrate
```
حال جهت ساخت دیتا در دیتا بیس کد را وارد کنید
```
php arrisan db:seed
```
برای داکر
```
docker-compose exec app php arrisan db:seed
```
حال برای استفاده فایل
```
lendo.postman_collection.json
```
را در پست من خود ایمورت کنید



حال در مدلهای خود کد زیر را اضافه کنید

 ```
 use App\Filter\Filterable;
 
 class Property extends Model
 {
     use Filterable;
 }
 ```

حال برای استفاده از سیستم فیلتر کد زیر را در کنترلر خود با استفاه از مدل فراخانی کنید

 ```
Property::filter($filter,$limit,['relation'])
 ```

$filter
همان فیلتر های 
Request
است که باید به متود پاس داده شود
$limit
مقدار آیتم های موجود در هر صفحه است
relation
نیز آرایه ای از ریلیشن های مدل فراخانی شده است ک در صورت نیاز میتوانید برای دریافت دیتا های ریلیشن ها و حذف ایگرلودینگ از این طریق روابط مدل را فراخوانی کنید

<br>
<br>
<br>




 ```
        "filters": [
            {
                "column": "title",
                "operator": "like",
                "value": "%o%",
                "relation": null
            },
            {
                "column": "name",
                "operator": "like",
                "value": "%North%",
                "relation": "city"
            }
        ],
        "orders": [
            {
                "column": "title",
                "operator": "desc"
            }, {
                "column": "id",
                "operator": "asc"
            }
        ]
   
 ```
برای ارسال فیلتر از طریق ریکويست باید با استاندارد بالا دیتا های خود را ارسال کنید

<br>
<br>

filters
همان کلید تمام فیلتراهاست و تمام فیلتر ها در ای قسمت نوشته میشود


<br>
<br>
orders
کلید تمام مرتب سازی هاست و برای سوتر کردن رکورد های جدول بر اساس یک فیلد خاص استفاده میشود
<br>
<br>
column
نام فیلدی که قرار است بر روی آن فیلتر و یا مرتب سازی صورت گیرد این فیلد باید یک از فیلدهای موجود در جدول و یا جدول های روابط باشد

<br>
<br>
operator
عملیات مورد نظر برای فیلتر کردن میباشد لیست عملیاتی که در حال حاضر قابل انجام است

 ```
=
<=
>=
!=
like
in
between
 ```
و برای مرتب سازی 
 ```
asc
desc
 ```

<br>
<br>
value
مقداری که برای فیلتر مورد نیاز است این قسمت برای برخی از عملگر ها مانند in و between میتواند آرایه بگیرد
 
<br>
<br>
relation
این فیلد اگر میخواهید عملیات را در جدول اصلی انجام دهید خالی باید باشد اما اگر میخواهید با استفاده از whereHas بر روی جداول ریلیشن فیلتر اعمال کنید نام ریلیشن را در ای قسمت وارد کنید

