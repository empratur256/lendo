<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 12:25 PM
 */

namespace App\Repo\Contracts;


use Illuminate\Database\Eloquent\Model;

interface RepoContract
{
    /**
     * @param array $data
     * @return Model
     */
    public function add(array $data):Model;

    /**
     * @param int $id
     * @return Model
     */
    public function find(int $id):Model;

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data);

    /**
     * @return Model
     */
    public function getModel():Model;

    /**
     * @param Model $model
     * @return mixed
     */
    public function setModel(Model $model);

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);

    /**
     * @param int $limit
     * @param array $filter
     * @return mixed
     */
    public function get(int $limit = 20, array $filter = []);

    /**
     * @return bool
     */
    public function canDelete():bool;
}