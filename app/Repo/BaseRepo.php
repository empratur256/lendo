<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 11:55 AM
 */

namespace App\Repo;


use App\Repo\Contracts\RepoContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepo implements RepoContract
{

    /**
     * @param Model
     */
    protected $model;

    /**
     * @param array $data
     * @return Model
     */
    public function add($data):Model
    {
        $this->model->fill($data)->save();
        return $this->getModel();
    }

    /**
     * @return Model
     */
    public function getModel():Model
    {
        return $this->model;
    }

    /**
     * @param $model
     * @return Model
     */
    public function setModel(Model $model):Model
    {
        return $this->model = $model;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function find(int $id):Model
    {
        return $this->setModel($this->model->newQuery()->find($id));
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function update(array $data)
    {
        return $this->model->update($data);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->model->newQuery()->whereId($id)->delete();
    }

    /**
     * @param int $limit
     * @param $filter
     * @return Collection
     */
    public function get(int $limit = 20 ,array $filter = [])
    {
        $query = $this->model->newQuery();
        if($limit){
            return $query->paginate($limit);
        }
        return $query->get();
    }

    /**
     * @return bool
     */
    abstract public function canDelete():bool;
}