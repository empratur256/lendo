<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/13/18
 * Time: 2:43 PM
 */

namespace App\Repo;


use App\Models\Property;
use Illuminate\Database\Eloquent\Collection;

class PropertyRepo extends BaseRepo
{
    /**
     * PropertyRepo constructor.
     */
    public function __construct()
    {
        $this->setModel(new Property());
    }

    /**
     * @return bool
     */
    public function canDelete():bool
    {
        return true;
    }


    /**
     * @param int $limit
     * @param array $filter
     * @return Collection|mixed
     * @throws \Exception
     */
    public function get(int $limit = 20, array $filter = [])
    {
        return Property::filter($filter["filter"]??[],$limit,['city']);
    }
}