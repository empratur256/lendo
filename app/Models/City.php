<?php

namespace App\Models;

use App\Filter\Contracts\ModelFilter;
use App\Filter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends Model implements ModelFilter
{
    use HasFactory,Filterable;

    /**
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * @return HasMany
     */
    public function property(){
        return $this->hasMany(Property::class);
    }
}
