<?php


namespace App\Filter;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

trait Filterable
{
    /**
     * @param array|null $filters
     * @param null $limit
     * @param array $with
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    static public function filter(array $filters = null, $limit = null,array $with = [])
    {
        try {
            $model = self::query();
            $model = new QueryMake($model);
            $model = $model->make($filters)->order($filters)->getQuery();
            return $limit ? $model->with($with)->paginate($limit) : $model->with($with)->get();
        } catch (Exception $e) {
            throw new Exception('The filter does not exist');
        }
    }
}