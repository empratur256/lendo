<?php


namespace App\Filter;


use App\Filter\FilterBuilder\Query;
use Illuminate\Database\Eloquent\Builder;

class QueryMake
{
    /**
     * @var string[]
     */
    private $operator = ["between" => 'between', "in" => 'in', "like" => 'wheres', "=" => 'wheres', "!=" => 'wheres', ">=" => 'wheres', "<=" => 'wheres'];
    private $query;

    /**
     * QueryMake constructor.
     * @param Builder $model
     */
    public function __construct(Builder $model)
    {
        $this->query = new Query($model);
    }

    /**
     * @param array|null $filters
     * @return $this
     */
    public function make(array $filters = null)
    {
        if (isset($filters['filters'])){foreach ($filters['filters'] as $item){
            if(isset($item['relation'])){
                $this->query = $this->query->relation($item);
            }else{
                $this->query = $this->query->{$this->operator[$item['operator']]}($item);
            }
        }}

        return $this;
    }

    /**
     * @param array|null $filters
     * @return $this
     */
    public function order(array $filters = null)
    {
        if (isset($filters['orders'])){
            foreach ($filters['orders'] as $item){
                $this->query = $this->query->orders($item);
            }

        }
        return $this;
    }

    /**
     * @return Builder
     */
    public function getQuery()
    {
        return $this->query->getQuery();
    }
}