<?php


namespace App\Filter\FilterBuilder;


use App\Filter\Contracts\MakeFilterBuilder;
use Illuminate\Database\Eloquent\Builder;

class Query implements MakeFilterBuilder
{
    /**
     * @var Builder
     */
    private $model;

    /**
     * FilterQuery constructor.
     * @param Builder $model
     */
    public function __construct(Builder $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $filter
     * @return $this|MakeFilterBuilder
     */
    public function wheres(array $filter): MakeFilterBuilder
    {
        $this->model = $this->model->where($filter["column"], $filter['operator'], $filter["value"]);
        return $this;
    }

    /**
     * @param array $filter
     * @return $this|MakeFilterBuilder
     */
    public function between(array $filter): MakeFilterBuilder
    {
        $this->model = $this->model->whereBetween($filter["column"], $filter["value"]);
        return $this;
    }

    /**
     * @param array $filter
     * @return $this|MakeFilterBuilder
     */
    public function in(array $filter): MakeFilterBuilder
    {
        $this->model = $this->model->whereIn($filter["column"], $filter["value"]);
        return $this;
    }

    /**
     * @param array $filter
     * @return $this|MakeFilterBuilder
     */
    public function orders(array $filter): MakeFilterBuilder
    {
        $this->model = $this->model->orderBy($filter["column"], $filter["operator"]);
        return $this;
    }

    /**
     * @param array $filter
     * @return $this|MakeFilterBuilder
     */
    public function relation(array $filter): MakeFilterBuilder
    {
        $this->model = $this->model->whereHas($filter['relation'], function (Builder $q) use ($filter) {
            $q->where($filter["column"], $filter['operator'], $filter["value"]);
        });
        return $this;
    }

    /**
     * @return Builder
     */
    public function getQuery():Builder
    {
        return $this->model;
    }
}