<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 12:25 PM
 */

namespace App\Filter\Contracts;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface MakeFilterBuilder
{
    /**
     * @param array $filter
     * @return MakeFilterBuilder
     */
    public function wheres(array $filter ):MakeFilterBuilder;

    /**
     * @param array $filter
     * @return MakeFilterBuilder
     */
    public function between(array $filter ):MakeFilterBuilder;

    /**
     * @param array $filter
     * @return MakeFilterBuilder
     */
    public function in(array $filter ):MakeFilterBuilder;

    /**
     * @param array $filter
     * @return MakeFilterBuilder
     */
    public function orders(array $filter ):MakeFilterBuilder;

    /**
     * @param array $filter
     * @return MakeFilterBuilder
     */
    public function relation(array $filter ):MakeFilterBuilder;

    /**
     * @return Builder
     */
    public function getQuery():Builder;
}