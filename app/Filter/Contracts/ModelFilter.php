<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/9/18
 * Time: 12:25 PM
 */

namespace App\Filter\Contracts;


use Illuminate\Database\Eloquent\Model;

interface ModelFilter
{
    /**
     * @param array|null $filters
     * @param null $limit
     * @return Model
     */
    static  public function filter(array $filters = null,$limit = null);
}