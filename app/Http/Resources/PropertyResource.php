<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'rental_price'=>$this->rental_price,
            'mortgage_price'=>$this->mortgage_price,
            'city'=>$this->city->name,
            'images'=>config('app.url').$this->images,
            'bedrooms'=>$this->bedrooms,

        ];
    }
}
