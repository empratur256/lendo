<?php

namespace App\Http\Controllers;

use App\Http\Resources\PropertyResource;
use App\Repo\PropertyRepo;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PropertyController extends MyController
{
    /**
     * @var PropertyRepo|null
     */
    protected $repo = null;

    /**
     * PropertyController constructor.
     * @param PropertyRepo $repo
     */
    public function __construct(PropertyRepo $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }


    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws \Exception
     */
    public function index(Request $request)
    {
        return PropertyResource::collection($this->repo->get($request->input('limit',20),$request->toArray())) ;
    }
}
