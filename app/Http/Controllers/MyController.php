<?php
/**
 * Created by PhpStorm.
 * User: meysam
 * Date: 5/7/18
 * Time: 2:35 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Config;

class MyController extends Controller
{
    /**
     * @var null
     */
    protected $repo = null;

    /**
     * MyController constructor.
     */
    public function __construct()
    {

    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->repo->find($id);
        if ($this->repo->canDelete()) {
            $this->repo->delete($id);
            return response()->json(true);
        }
        return response()->json(false, 409);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->show($id);
    }


    /**
     *
     */
    public function create()
    {
        //
    }
}