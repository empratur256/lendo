<?php

namespace Tests\Feature;


use App\Models\Property;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    public function testResponseStructure()
    {
        \App\Models\City::factory(2)->create();
        \App\Models\Property::factory(10)->create();


        $response = $this->getJson('/properties?limit=5');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'rental_price',
                    'mortgage_price',
                    'city',
                    'images',
                    'bedrooms',
                ],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response = $this->getJson('/properties');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'rental_price',
                    'mortgage_price',
                    'city',
                    'images',
                    'bedrooms',
                ],
            ],
        ]);
    }


    public function testFilter()
    {
        \App\Models\City::factory(2)->create();
        \App\Models\Property::factory(20)->create();

        $response = $this->getJson('/properties?filter[filters][0][column]=title&filter[filters][0][operator]=like&filter[filters][0][value]=%o%&filter[filters][0][relation]=&filter[filters][1][column]=name&filter[filters][1][operator]=like&filter[filters][1][value]=%North%&filter[filters][1][relation]=city&filter[orders][0][column]=title&filter[orders][0][operator]=desc&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);

        $response = $this->getJson('/properties?filter[filters][0][column]=title&filter[filters][0][operator]=like&filter[filters][0][value]=%o%&filter[filters][0][relation]=&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);

        $response = $this->getJson('/properties?filter[filters][1][column]=name&filter[filters][1][operator]=like&filter[filters][1][value]=%North%&filter[filters][1][relation]=city&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);
        $response = $this->getJson('/properties?filter[filters][2][column]=rental_price&filter[filters][2][operator]=between&filter[filters][2][value][0]=1000&filter[filters][2][value][1]=200000&filter[filters][2][relation]=&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);
        $response = $this->getJson('/properties?filter[filters][3][column]=mortgage_price&filter[filters][3][operator]=between&filter[filters][3][value][0]=100000&filter[filters][3][value][1]=20000000&filter[filters][3][relation]=&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);
        $response = $this->getJson('/properties?filter[filters][4][column]=images&filter[filters][4][operator]=!=&filter[filters][4][value]=&filter[filters][4][relation]=&filter[orders][1][column]=id&filter[orders][1][operator]=asc&limit=5');

        $response->assertStatus(200);
    }
}
