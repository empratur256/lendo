<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Property::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $file = UploadedFile::fake()->image($this->faker->name.'.jpg');
        $file = $file->move('storage/app/public',$file->hashName());
        return [
            'title'=> $this->faker->name,
            'rental_price'=> $this->faker->numberBetween(0,2000000),
            'mortgage_price'=> $this->faker->numberBetween(0,200000000),
            'city_id'=> $this->faker->numberBetween(City::min('id'),City::max('id')),
            'images'=> Storage::url($file->getFilename()),
            'bedrooms'=> $this->faker->numberBetween(1,5),
        ];
    }
}
